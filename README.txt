CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Uninstalling
 * Frequently Asked Questions (FAQ)
 
INTRODUCTION
-----------
To create a page Drupal needs to make several database queries. This can slow down websites with a lot of traffic. To make websites faster Drupal stores web pages in a cache.

Clearing caches are relatively harmless. Sites might slow down for a bit afterward while the cache fills back up.

So if you have updated something on node pages but it won't be displayed to an anonymous user until cache cleared, but for single changes, it's not a good idea to clear full drupal cache.

Drupal not giving an ability to clear cache for a single page so this module will give you button on every page so you can clear cache for every single term page and node page.

Once you install this module you will get button on "clear cache for single page" near to "view" and "edit" page button. Once you done you change, just click on this button and you will be able to show latest change for the same page to anonymous users. 

REQUIREMENTS
------------

INSTALLATION
----------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.

Navigate to administer >> modules. Enable Views and Views UI.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.

UNINSTALLING
-------------
